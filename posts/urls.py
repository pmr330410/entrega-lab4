from django.urls import path

from . import views

app_name = 'posts'
urlpatterns = [
    #path('', views.list_posts, name='index'),
    path('', views.ListPost.as_view(), name='index'),
    #path('<int:post_id>/', views.detail_post, name='detail'),
    path('<int:pk>/', views.DetailPost.as_view(), name='detail'),
    #path('create/', views.create_post, name='create'),
    path('create/', views.CreatePost.as_view(), name='create'),
    #path('update/<int:post_id>/', views.update_post, name='update'),
    path('update/<int:pk>/', views.UpdatePost.as_view(), name='update'),
    #path('delete/<int:post_id>/', views.delete_post, name='delete'),
    path('delete/<int:pk>/', views.DeletePost.as_view(), name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('categories/', views.CategoryListView.as_view(), name='categories'),
    path('categories/<int:category_id>', views.detail_category, name='detail-category'),
    path('categories/create', views.CategoryCreateView.as_view(), name='create-category'),
]