from django.db import models
from django.conf import settings

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    game_type = models.CharField(max_length=255)
    description = models.TextField()
    #posts = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.game_type} by {self.author}'

class Game(models.Model):
    name = models.CharField(max_length = 60)

    def __str__(self):
        return f'{self.name}'

class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE) #autor
    title = models.CharField(max_length=60) #título
    text = models.TextField() #conteúdo
    post_date = models.DateTimeField(auto_now=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category)

    def __str__(self):
        return f'"{self.title}" - {self.game}'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.text} - {self.author.username}'




