from this import d
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.views import generic

# def detail_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)
#     context = {'post': post, 'game': post.game}
#     return render(request, 'posts/detail.html', context)

# def list_posts(request):
#     post_list = Post.objects.all()
#     context = {'post_list': post_list}
#     return render(request, 'posts/index.html', context)

# def update_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)

#     if request.method == "POST":
#         form = PostForm(request.POST)
#         if form.is_valid():
#             post.title = form.cleaned_data['title']
#             post.text = form.cleaned_data['text'] # Renderizado com a tag 'safe' no HTML
#             post.game = form.cleaned_data['game']
#             post.save()
#             return HttpResponseRedirect(
#                 reverse('posts:detail', args=(post.id, )))

#     else:
#         form = PostForm(
#             initial={
#                 'title': post.title,
#                 'text': post.text,
#                 'game': post.game
#             }
#         )
#     context = {'post': post, 'form': form}
#     return render(request, 'posts/update.html', context)

# def delete_post(request, post_id):
#     post = get_object_or_404(Post, pk=post_id)

#     if request.method == "POST":
#         post.delete()
#         return HttpResponseRedirect(reverse('posts:index'))

#     context = {'post': post}
#     return render(request, 'posts/delete.html', context)

class CreatePost(generic.CreateView):
    model = Post
    template_name = 'posts/create.html'
    fields = ['author', 'title', 'text', 'game', 'categories']
    success_url = reverse_lazy('posts:index')

class DetailPost(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class ListPost(generic.ListView):
    model = Post
    template_name = 'posts/index.html'

class UpdatePost(generic.UpdateView):
    model = Post
    template_name = 'posts/update.html'
    fields = ['author', 'title', 'text', 'game', 'categories']
    success_url = reverse_lazy('posts:index')

class DeletePost(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    success_url = reverse_lazy('posts:index')


def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/categories.html'

class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'posts/create_category.html'
    fields = ['author', 'game_type', 'description']
    success_url = reverse_lazy('posts:categories')

def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'posts/detail_category.html', context)